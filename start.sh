#!/bin/bash
#
# Script executed in the docker file to resgister and run the Gitlab runner.

if [ "$GITLAB_RUNNER_EXECUTOR" = 'docker' ]
then
    gitlab-runner register -n -r "$GITLAB_RUNNER_REGISTRATION_TOKEN" \
        --name "$GITLAB_RUNNER_DESCRIPTION" \
        --tag-list "$GITLAB_TAG_LIST" \
        --url "$GITLAB_RUNNER_COORDINATOR_URL" \
        --executor "$GITLAB_RUNNER_EXECUTOR" \
        --docker-image "docker:latest" \
        --docker-privileged
else
    gitlab-runner register -n -r "$GITLAB_RUNNER_REGISTRATION_TOKEN" \
        --name "$GITLAB_RUNNER_DESCRIPTION" \
        --tag-list "$GITLAB_TAG_LIST" \
        --url "$GITLAB_RUNNER_COORDINATOR_URL" \
        --executor "$GITLAB_RUNNER_EXECUTOR" \
        --shell "$GITLAB_RUNNER_SHELL"
fi

exec gitlab-runner run --user=gitlab-runner --working-directory=/home/gitlab-runner
