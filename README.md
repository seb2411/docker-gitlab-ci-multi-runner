# Docker container for Gitlab CI Runner

A simple docker container to run and register a Gitlab CI Runner.

You have to pass some environment variables to declare a new Runner:

Environment variable             | Required | Description
-------------------------------- | -------- | -----------
GITLAB_RUNNER_COORDINATOR_URL    |    YES   | Complete Url of the Gitlab CI (Ej: https://gitlab.com/ci)
GITLAB_RUNNER_REGISTRATION_TOKEN |    YES   | The registration token from your Gitlab
GITLAB_RUNNER_DESCRIPTION        |     NO   | A description for the Gitlab Runner
GITLAB_RUNNER_EXECUTOR           |    YES   | A Gitlab Runner Executor ('docker' or 'shell')
GITLAB_RUNNER_SHELL              |    YES   | A Gitlab Runner Shell ('bash', 'sh', 'cmd', 'powershell')
GITLAB_TAG_LIST                  |     NO   | Tags for the runner separated by commas


**Docker executor:** With docker executor the default image is the docker:latest and docker is running in privileged mode.
