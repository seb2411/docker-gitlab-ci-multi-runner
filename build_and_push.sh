#!/bin/bash

# Copyright (c) `2017` Sebastien Lemarinel
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."

# Build and push a docker image.

REGISTRY_URL='registry.gitlab.com'
PROJECT_URL='seb2411/docker-gitlab-ci-multi-runner/image'
DEFAULT_TAG='latest'

echo "Do you want to add a tag (use only 'latest') ? (yes/no)"
read reponse

if [[ "$reponse" == "yes" ]]
then
    echo "Your tag:"
    read tag

    eval 'docker build -t ${REGISTRY_URL}/${PROJECT_URL}:${tag} .'
fi

eval 'docker build -t ${REGISTRY_URL}/${PROJECT_URL}:${DEFAULT_TAG} .'

if [[ "$reponse" == "yes" ]]
then
    eval 'docker push ${REGISTRY_URL}/${PROJECT_URL}:${tag}'
fi

eval 'docker push ${REGISTRY_URL}/${PROJECT_URL}:${DEFAULT_TAG}'
